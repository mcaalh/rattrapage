/**
 * Reservation.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },

    start_at: {
      type: 'datetime',
      required: true
    },

    end_at: {
      type: 'datetime',
      required: true
    },

    activities_ids: {
      collection: 'activity',
      via: 'reservations_ids'
    },

    createdAt: {
        type: 'datetime',
        columnName: 'created_at'
    },

    updatedAt: {
        type: 'datetime',
        columnName: 'updated_at'
    },

    cart_id: {
      model: 'cart'
    },

    bill_id: {
      model: 'bill'
    },
    // Add a reference to Users
    users: {
        collection: 'user',
        via: 'reservations_ids'
    },

  }
};

