/**
 * Bill.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    
    label: {
      type: 'string'
    },
    amount: {
      type: 'float'
    },
    paid: {
      type: 'boolean',
    },

    createdAt: {
      type: 'datetime',
      columnName: 'created_at'
    },

    updatedAt: {
      type: 'datetime',
      columnName: 'updated_at'
    },

    reservation_id: {
      model: 'reservation'
    },
    
    user_id: {
      model: 'user'
    }
  }
};

