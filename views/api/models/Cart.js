/**
 * Cart.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    activity_id: {
      model: 'activity'
    },
    order_id: {
      model: 'order'
    },

    activities_ids: {
      collection: 'activity',
      // via: 'cart_id'
    },
    reservations_ids:{
      collection: 'reservation',
      via: 'cart_id'
    },
    user_id: {
      model: 'user'
    },
    item_count: {
      type: 'integer'
    },
    price: {
      type: 'float'
    },
  }
};

