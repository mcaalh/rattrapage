/**
 * Comment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    comment: {
      type: 'text',
      required: true
    },

    activity_id: {
      model: 'activity',
      required: true
    },

    createdAt: {
        type: 'datetime',
        columnName: 'created_at'
    },

    updatedAt: {
        type: 'datetime',
        columnName: 'updated_at'
    },

    // Add a reference to Users
    user_id: {
      model: 'user',
      required: true
    },
  }
};

