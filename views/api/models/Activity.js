/**
 * Activity.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },

    cart_id: {
      model: 'cart'
    },

    description: {
      type: 'string',
      required: true
    },

    price: {
      type: 'float',
      required: true
    },

    level: {
      type: 'string',
      required: false
    },

    max_user: {
      type: 'string',
      required: false
    },

    cover: {
      type: 'string'
    },

    // covers: {
    //   collection: 'cover',
    //   via: 'activity_id',
    // },

    category: {
      type: 'string',
    },

    start_at: {
      type: 'datetime',
      // required: true
    },

    end_at: {
      type: 'datetime',
      // required: true
    },

    reservations_ids: {
      collection: 'reservation',
      via: 'activities_ids'
    },

    comments_ids: {
      collection: 'comment',
      via: 'activity_id'
    },

    itinerary_id: {
      model: 'itinerary',
      // required: true
    },

    pictures_ids: {
      collection: 'picture',
      via: 'activity_id',
    },

    createdAt: {
        type: 'datetime',
        columnName: 'created_at'
    },

    updatedAt: {
        type: 'datetime',
        columnName: 'updated_at'
    },

    // Add a reference to Users
    users: {
        collection: 'user',
        via: 'activities_ids'
    },
  }
};

