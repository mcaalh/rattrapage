/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var bcrypt = require('bcrypt');

module.exports = {
    'new': function (req, res) {
        // req.session.authenticated = true;
        // console.log(req.session);
        res.view('/login');
    },
    create: function (req, res, next) {

        if (!req.param('email') || !req.param('password')) {
            console.log(req.session);
            var usernamePasswordRequiredError = [{
                name: 'usernamePasswordRequired', message: 'you must enter both password and email'
            }];
            
            req.session.flash = {
                err: usernamePasswordRequiredError
            }
            res.redirect('/login');
            return;
        }
        User.findOne({ email: req.param('email') }).exec(function (err, user) {
            console.log("found");
            if (err) {
                return next(err);
            }
            if (!user) {
                var noAccountError = [{ name: 'noAccount', message: 'The email address' + req.param('email' + ' not found.') }];
                req.session.flash = {
                    err: noAccountError
                }
                res.redirect('/login');
                return;
            }
            bcrypt.compare(req.param('password'), user.password, function (err, valid) {
                if (err) {
                    return next();
                }
                if (!valid) {
                    var usernamePasswordMismatchError = [{ name: 'usernamePasswordMismatch', message: 'Invalid username and password combination' }]
                    req.session.flash = {
                        err: usernamePasswordMismatchError
                    }
                    res.redirect('/login');
                    return;
                }

                req.session.authenticated = true;
                req.session.User = user;

                user.online = true;
                user.save(function(err, saveUser){
                    console.log("inside create session ",user);
                    if (err) {
                        return (next(err));
                    }
                    if (req.session.User.admin) {
                        res.redirect('/dashboard');
                        return;
                    }
                    if (!req.session.User.admin) {
                        res.redirect('/user/show/' + user.id);
                        return;
                    }
                    req.session.flash = {};
                })

                
                
                console.log('req.session');
                // console.log(req.session);

            });
        });
    },
    logout: function (req, res, next) {
        req.session.destroy(function (err) {
            if (err) {
                return next(err);
            }
            return res.redirect('/');
        });
    },
    destroy: function (req, res, next) {
        User.findOne(req.session.User.id, function foundUser (err, user) {
            var userId = req.session.User.id;

            User.update(userId, 
                { 
                    online:false 
                }, function (err) {
                if (err) {
                    return next(err);
                }
                req.session.destroy();
                res.redirect('/');
            });
        });
    }
};

