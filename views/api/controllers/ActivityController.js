/**
 * ActivityController
 *
 * @description :: Server-side logic for managing activities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var moment = require('moment');

module.exports = {
    indexActivity: async function (req, res, next) {

        console.log("get activities");

        var terre = await Activity.find({ category: 'terre' });
        var air = await Activity.find({ category: 'air' });
        var eau = await Activity.find({ category: 'eau' });
        var simulation = await Activity.find({ category: 'simulation' });
        // console.log(terre,"////////////\n", air,"////////////\n", eau,"////////////\n", simulation,"////////////\n");
        
        return res.view(
            'activity',
            {
                activities: {
                    terre: terre,
                    air: air,
                    eau: eau,
                    simulation: simulation
                }
            }
        );
    },
    showResa: function (req, res, next) {
        console.log('show resa ', req.param('id'));
        Activity.findOne(req.param('id')).exec(function (err, foundResa) {
            if (err) return res.serverError(err);
            if (!foundResa) return res.notFound();
            res.view(
                'order/reservation/showResa',{
                activity: foundResa
            });
        });
    },

    uploadCoverAndCreate: function(req, res, next) {
        console.log(req.params.all());
        console.log("params");
        if (req.method === 'GET')
            return res.json({ 'status': 'GET not allowed' });
        //  Call to /upload via GET is error
        var reqData = {

        }
        var pictureUpload = req.file('file');

        // console.log(req.param('picture'));
        // console.log(pictureUpload);

        let dirname = '../../assets/uploaded/cover/activity';
        pictureUpload.upload({ dirname: dirname}, function onUploadComplete(err, files) {
            if (err) {
                return res.serverError(err);
            }
                
            let source = files[i]['fd'].match(/[^\/]+$/)[0];
            var data = {
                "source": source,
                "name": files[i]['filename'],
                // "createdBy": req.params.id,
                // "updatedBy": req.params.id
            };
            Cover.create(data).exec(function (err, pic) {
                if (err) {
                    console.log('error creating pic');
                    return res.redirect('/dashboard/pictures');
                }

                console.log('pic created: ', pic.name);
            });
            
            return res.redirect('/dashboard/pictures');
        });
    },

	createActivity: function (req, res, next) {
        var data = req.params.all();
        console.log("create Activity data", data);

        Activity.create(data, function (error, activity){
            // console.log(activity);
            if ( error ) {
                console.log(error);
            }
            else{
                return res.redirect("/dashboard/activity");
            }
        });
    },
    "getAllActivities": function (req, res, next) {
        Activity.find({})
        .exec(function(err, activities){
            if (err) {
                return next(err);
            }
            
            for (var i = 0; i < activities.length; i++) {

                activities[i].start_at = moment(activities[i].start_at).format("DD/MM/YYYY");
                activities[i].end_at = moment(activities[i].end_at).format("DD/MM/YYYY");
            }
            console.log(activities);
            return res.view(
                'admin/pages/activity/list',
                {
                activities: activities
            }

            );
        });
    },
    "getActivity": function (req, res, next) {
        var id = req.param("id");
        // console.log(id);
        Activity.findOne(id)
        .exec(function(err, activity){
            if (err) {
                return next(err);
            }
            var dateDebut = moment(activity.start_at).format("DD/MM/YYYY");
            var dateFin = moment(activity.end_at).format("DD/MM/YYYY");
            activity.start_at = dateDebut;
            activity.end_at = dateFin;
            return res.view(
                'admin/pages/activity/edit',
                {
                activity: activity
            }
            );
        });
    },

    "edit": function(req, res, next){
        var id = req.param("id");
        // console.log("id" + id);
        var data = req.params.all();

        Activity.update(id, data)
        .exec(function(error, activity){
            if ( error ) {
                return console.log("erreur : ", error);
            }
            else{

                return res.redirect('dashboard/activity');
                // res.view('admin/pages/activity/edit',
                // {
                //     activity: activity
                // });
            }
        });
    },
    "delete": function(req, res, next){
        var id = req.param("id");
        console.log("id", id);
        Activity.destroy(id)
        .exec(function(error, activity){
            if ( error ) {
                return console.log("erreur : ", error);
            }
            else{
                return res.redirect("/dashboard/activity");
            }
        });
    }
};
