/**
 * PictureController
 *
 * @description :: Server-side logic for managing pictures
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    "getAllPictureJson": function (req, res, next) {
        Picture.find({})
        .exec(function(err, pics){
            if (err) {
                return next(err);
            }
            console.log(pics);
            return res.view(
                'admin/pages/pictures/list',
                {
                pictures: pics
            }

            );
        });
    },
    
    "edit": function (req, res, next) {
        var id = req.param('id');
        console.log(id);
        data = {};
        data.name = req.param("name");
        data.category = req.param("category");
        console.log("data", data);
        Picture.update(id, data)
            .exec(function(err, pics){
                if (err) {
                    return next(err);
                }
                console.log("pics update ", pics);
            return res.redirect("/dashboard/pictures");
        });
    },
	"getAllPicture": function (req, res, next) {
        Picture.find({})
        .exec(function(err, pics){
            if (err) {
                return next(err);
            }
            console.log(pics);
            return res.view(
                'me/about',
                {
                pictures: pics
            });
        });
    },
    upload: function(req, res, next) {
        console.log(req.params.all());
        console.log("params");
        if (req.method === 'GET')
            return res.json({ 'status': 'GET not allowed' });
        //  Call to /upload via GET is error

        var pictureUpload = req.file('file');

        // console.log(req.param('picture'));
        // console.log(pictureUpload);

        let dirname = '../../assets/uploaded/documents';
        pictureUpload.upload({ dirname: dirname}, function onUploadComplete(err, files) {
            //  Files will be uploaded to .tmp/uploads
            console.log(files);
            if (err) return res.serverError(err);
            //  IF ERROR Return and send 500 error with error

            for (var i = 0; i < files.length; i++) {
                
                let source = files[i]['fd'].match(/[^\/]+$/)[0];
                var data = {
                    "source": source,
                    // "source" : files[0]['fd'],
                    "size": files[i]['size'],
                    "ext": files[i]['type'],
                    "name": files[i]['filename'],
                    "description": req.param('description'),
                    "category": req.param('category')
                    // "createdBy": req.params.id,
                    // "updatedBy": req.params.id
                };
                Picture.create(data).exec(function (err, pic) {
                    if (err) {
                        console.log('error creating pic');
                        return res.redirect('/dashboard/pictures');
                    }
                    console.log('pic created: ', pic.name);
                });
            }
            return res.redirect('/dashboard/pictures');
        });
    },
};

