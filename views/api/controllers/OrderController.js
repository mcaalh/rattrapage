/**
 * OrderController
 *
 * @description :: Server-side logic for managing orders
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    addOrder: function (req, res, next) {
        if (!req.session.User){
            return res.redirect('/login');
        }
        if (!req.session.Cart){
            return res.redirect('/');
        }
        Cart.findOne(req.session.Cart.id)
            .populate('activities_ids')
            .exec(function (err, cart) {
                if (err) return res.send("pas de produit dans le panier");
                if (!cart) return res.notFound();
                console.log(cart);
                // This message can contain anything you want!
                // User.message(cart.id, {count: 12, hairColor: 'red'});

                // return res.ok();
                
                console.log("order recap  ", cart);
                return res.view("order/recap", {
                    layout: "soExtreme/layout",
                    cart: cart
                });
            });

        // var cart = req.session.Cart;
        
    }
};

