/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = async function(cb) {
  // console.log("Bootstrap");
  var data = {
    "firstname": "furious",
    "lastname": "duck",
    "email": "admin@test.com",
    "password": "passFurious",
    "admin": true,
  }
  var user = await User.find({ email: "admin@test.com"});
  // console.log("Bootstrap", user);
  if (user.length == 0){
    console.log("no admin user");
    User.create(data, function userCreated(err, user) {
      if (err) {
        console.log(err);
        return err;
      }
      console.log("admin user created", user);
    });
  }
  console.log("admin user already created");
  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};
