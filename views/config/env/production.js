/**
 * Production environment settings
 *
 * This file can include shared settings for a production environment,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/
  connections: {
    mongOnline: {
      adapter: 'sails-mongo',
      host: 'ds149763.mlab.com',
      port: 49763,
      user: 'bool',
      password: 'aSpO4v86', 
      database: 'bigproject'
    },
  },

  models: {
    connection: 'mongOnline',
    migrate: 'safe'
    //migrate: 'safe'
  },

  // appUrl: 'http://localhost:1337',
  // frontUrl: 'http://localhost:4200',

  // models: {
  //   connection: 'someMongodbServer'
  // }
  port: 1983,
};
