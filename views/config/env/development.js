/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/
  connections: {
    mongOnline: {
      adapter: 'sails-mongo',
      host: 'ds149763.mlab.com',
      port: 49763,
      user: 'bool',
      password: 'aSpO4v86', 
      database: 'bigproject'
    },
    mongoLocal:{
      adapter: 'sails-mongo',
      host: 'localhost',
      port: 27017,
      database: 'bigproject'
    },
    mysqlServer: {
      adapter: 'sails-mysql',
      host: 'localhost',
      user: 'root', //optional
      password: 'brennus', //optional
      database: 'bigproject' //optional
    }
  },
  
  models: {
    connection: 'mysqlServer',
    migrate: 'alter'
    //migrate: 'safe'
  },

  // appUrl: 'http://localhost:1337',
  // frontUrl: 'http://localhost:4200',

  port: 1337,
};