/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  // '/': {
  //   locals: {
  //     layout: 'newLayout'
  //   },
  //   view: 'order/reservation/product'
  // },

  // 'get /': 'ActivityController.indexActivity',
  'GET /cgv': { view: 'CGV',
    locals: {
      layout: 'soExtreme/layout'
    } },
  'GET /mentions-legales': {
    view: 'mentions',
    locals: {
      layout: 'soExtreme/layout'
    }
  },

  'GET /details': {
    view: 'details',
    locals: {
      layout: 'soExtreme/layout'
    }
  },

  'GET /validated': {
    view: 'validated',
    locals: {
      layout: 'soExtreme/layout'
    }
  },

  'get /user/infos/:id': {
    // policy: 'admin',
    controller: "UserController",
    action: "infos",
    locals: {
      layout: 'soExtreme/layout'
    }
  },

  'get /user/orders/:id': {
    // policy: 'admin',
    controller: "UserController",
    action: "orders",
    locals: {
      layout: 'soExtreme/layout'
    }
  },

  'get /user/adresse/:id': {
    // policy: 'admin',
    controller: "UserController",
    action: "adresse",
    locals: {
      layout: 'soExtreme/layout'
    }
  },



  'get /user/show/:id': {
    // policy: 'admin',
    controller: "UserController",
    action: "show",
    locals: {
      layout: 'soExtreme/layout'
    }
  },
  'get /activity/showresa/:id': {
    // policy: 'admin',
    controller: "ActivityController",
    action: "showResa",
    // view: 'admin/pages/activity/list',

    locals: {
      layout: 'soExtreme/layout'
    }
  },

  'get /': {
    // policy: 'admin',
    controller: "ActivityController",
    action: "indexActivity",
    // view: 'admin/pages/activity/list',

    locals: {
      layout: 'soExtreme/layout'
    }
  },

  'get /login': {
    view: 'session/new'
  },

  'post /login': 'SessionController.create',

  // '/logout': 'AuthController.logout',
  '/logout': 'SessionController.logout',

  'get /register': {
    view: 'user/new'
  },

  'get /dashboard': {
    policy: 'admin',
    locals: {
        layout: 'admin/layout'
      },
    view: 'admin'
  },

  /** ACTIVITIES **/

  'get /dashboard/activity': {
    policy: 'admin',
    controller: "ActivityController",
    action: "getAllActivities",
    // view: 'admin/pages/activity/list',
    
    locals: {
        layout: 'admin/layout'
      }
  },

  'get /dashboard/activity/create': {
    policy: 'admin',
    locals: {
        layout: 'admin/layout'
      },
    view: 'admin/pages/activity/create'
  },

  'get /dashboard/activity/edit/:id': {
    policy: 'admin',
    controller: "ActivityController",
    action: "getActivity",
    locals: {
        layout: 'admin/layout'
      },
  },

  'post /dashboard/activity/create': 'ActivityController.createActivity',
  'post /dashboard/activity/edit/:id': 'ActivityController.edit',
  'get /dashboard/activity/delete/:id': 'ActivityController.delete',

  /** End ACTIVITIES **/

  ///**   Pictures   **/
  'get /dashboard/pictures': {
    // policy: 'admin',
    controller: "PictureController",
    action: "getAllPictureJson",
    locals: {
        layout: 'admin/layout'
      }
  },

  'get /dashboard/pictures/edit:id': {
    // policy: 'admin',
    locals: {
        layout: 'admin/layout'
      }
  },

  'get /dashboard/pictures/add': {
    // policy: 'admin',
    locals: {
        layout: 'admin/layout'
      },
    view: 'admin/pages/pictures/add'
  },

  'post /dashboard/pictures/create': 'PictureController.upload',
  'post /dashboard/pictures/edit/:id': 'PictureController.edit',

  ///** End  Pictures   **/
    ///**   Users   **/
    'get /dashboard/users-list': {
      policy: 'admin',
      controller: "UserController",
      action: "getAllUser",
      locals: {
          layout: 'admin/layout'
        }
    },
  
    'get /dashboard/user-edit/:id': {
      controller: "UserController",
      action: "edit",
      policy: 'admin',
      locals: {
          layout: 'admin/layout'
        }
    },




    // *********CART/////

  'get /view-cart': {
    // policy: 'admin',
    controller: "CartController",
    action: "viewCart",
    // view: 'admin/pages/activity/list',

    locals: {
      layout: 'soExtreme/layout'
    }
  }, 
  'get /pass-order': {
  // policy: 'admin',
  controller: "OrderController",
    action: "addOrder",
      // view: 'admin/pages/activity/list',

      locals: {
    layout: 'soExtreme/layout'
  }
},

    ///////end CART/////
    /** TEST */
    // 'get /say/paypal': 'TestController.paypal',
    'get /cart/return' : 'SayController.executePaypal',
  
    // 'get /dashboard/users/add': {
    //   policy: 'admin',
    //   locals: {
    //       layout: 'admin/layout'
    //     },
    //   view: 'admin/pages/users/add'
    // },
  
    // 'post /dashboard/users/create': 'PictureController.upload',
    // 'post /dashboard/users/edit/:id': 'PictureController.edit',
  
    ///** End  Pictures   **/

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
