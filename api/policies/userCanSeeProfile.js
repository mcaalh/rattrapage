module.exports = async function (req, res, ok) {
    console.log("req params");
    console.log(req.param('id'));
    console.log(req.session.User.id, "matchid");
    var sessionUserMatchesId = req.session.User.id == req.param('id');
    var isAdmin = await req.session.User.admin;

    console.log("sessionUserMatchesId ? " , sessionUserMatchesId);
    console.log("admin ? " , isAdmin);

    // the rq id does not match to the user id

    if(!sessionUserMatchesId) {
        if (isAdmin){
            return ok();
        }
        console.log("not user to see this");
        var noRigthsError = [{ name: 'noRigths', message: 'You must be an admin.' }];
        req.session.flash = {
            err: noRigthsError
        };
        res.redirect('/login');
        return;
    }
    
    return ok();
};