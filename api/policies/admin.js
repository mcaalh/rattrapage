//admin POLICIES
module.exports = function (req, res, ok) {
   
    if (req.session.User && req.session.User.admin) {
        console.log("isAdmin");
        return ok();
    }

    else {
        console.log("not Admin policies");
        var requireAdminError = [{name: 'requireAdminError', message: 'You don t have permission for that'}];
        req.session.flash = {
            err: requireAdminError
        }

        res.redirect('/login');
        return;
    }
};