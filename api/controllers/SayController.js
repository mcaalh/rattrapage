/**
 * SayController
 *
 * @description :: Server-side logic for managing says
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var paypal = require('paypal-rest-sdk');

module.exports = {
    hello: function (req, res) {

        console.log("say heloo");

        // Make sure this is a socket request (not traditional HTTP)
        if (!req.isSocket) {
            return res.badRequest();
        }

        // Have the socket which made the request join the "funSockets" room.
        sails.sockets.join(req, 'funSockets');

        // Broadcast a notification to all the sockets who have joined
        // the "funSockets" room, excluding our newly added socket:
        sails.sockets.broadcast('funSockets', 'hello', { howdy: 'hi there!' }, req);

        // ^^^
        // At this point, we've blasted out a socket message to all sockets who have
        // joined the "funSockets" room.  But that doesn't necessarily mean they
        // are _listening_.  In other words, to actually handle the socket message,
        // connected sockets need to be listening for this particular event (in this
        // case, we broadcasted our message with an event name of "hello").  The
        // client-side you'd need to write looks like this:
        // 
        // io.socket.on('hello', function (broadcastedData){
        //   console.log(data.howdy);
        //   // => 'hi there!'
        // }
        // 

        // Now that we've broadcasted our socket message, we still have to continue on
        // with any other logic we need to take care of in our action, and then send a
        // response.  In this case, we're just about wrapped up, so we'll continue on

        // Respond to the request with a 200 OK.
        // The data returned here is what we received back on the client as `data` in:
        // `io.socket.get('/say/hello', function gotResponse(data, jwRes) { /* ... */ });`
        return res.json({
            anyData: 'we want to send back'
        });

    },
    "paypal": function (req, res, next) {
        console.log("paypal actvated");
        var data = req.params.all();
        console.log(data);
        paypal.configure({
            'mode': 'sandbox', //sandbox or live
            'client_id': 'AZsMAtB5yoPnJNNMt5Sb0yS51hWz4gDkMlAZPCsRYXC0o7Nt2sTpoODIT6-lXVVHDQvSfxyuXcfIvJVt',
            'client_secret': 'EAbb_bFk0oOwMFH6DfEqJUl1nE84StboE2-ZgHojoJJzea8oNpEl-t01tKh8iZAfUP_owwC40Sssi25X'
        });
        
        var payReq = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": "http://localhost:1337/cart/return",
                "cancel_url": "http://localhost:1337/cart/cancel"
            },
            "transactions": [{
                "item_list": {
                    "items": [{
                        "name": "test Activity",
                        "sku": "activityTest",
                        "price": data.ammount,
                        "currency": "EUR",
                        "quantity": 1
                    }]
                },
                "amount": {
                    "currency": "EUR",
                    "total": data.ammount
                },
                "description": " paiement pour l'activité"
            }]
        };

        paypal.payment.create(payReq, function (error, payment) {
            var links = {};

            if (error) {
                console.error(JSON.stringify(error));
            } else {
                // Capture HATEOAS links
                payment.links.forEach(function (linkObj) {
                    links[linkObj.rel] = {
                        href: linkObj.href,
                        method: linkObj.method
                    };
                })

                // If redirect url present, redirect user
                if (links.hasOwnProperty('approval_url')) {
                    //REDIRECT USER TO links['approval_url'].href
                    res.redirect(links['approval_url'].href);
                } else {
                    console.error('no redirect URI present');
                }
            }
        });

    },
    executePaypal: function (req, res, next) {
        var paymentId = req.query.paymentId;
        var payerId = { payer_id: req.query.PayerID };
        console.log('--------------------------->payment id request',paymentId);
        paypal.payment.execute(paymentId, payerId, function (error, payment) {
            if (error) {
                console.error(JSON.stringify(error));
            } else {
                if (payment.state == 'approved') {
                    console.log('payment completed successfully');
                    req.session.Cart = null;
                    return res.redirect('/validated');
                } else {
                    console.log('payment not successful');
                }
            }
        });
    }
};

