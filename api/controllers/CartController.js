/**
 * CartController
 *
 * @description :: Server-side logic for managing carts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	addToCart: function (req, res, next) {
        var data = req.params.all();
        data.activities_ids = [data.activity_id];
        var activities = [];
        console.log("create cart data", req.session.Cart);
        var activity = Activity.find(data.activity_id);
        if (req.session.User){
            data.user_id = req.session.User.id;
            // Cart.update(req.session.Cart, )
        }
        if (req.session.Cart) {
            console.log("update =============================== cart data", data.activity_id);
            
            // Cart.update(req.session.Cart.id, {activities_ids: activity.id})
            //     .exec(function (err, cart) {
            //         if (err){
            //             return res.serverError(err);
            //         }
            //         console.log("cart is updated ", cart);
            //         return res.redirect("/");
            //     })
            Cart.findOne(req.session.Cart.id)
                .exec(function (err, cart) {
                    if (err) {
                        return res.serverError(err);
                    }
                    if (!cart) { 
                        return res.notFound('Could not find a user named Finn.'); 
                    }
                    cart.activities_ids.add(data.activity_id);
                    cart.save(function (save) {
                        if (err) { return res.serverError(err); }
                        console.log("cart is updated ", cart);
                        return res.redirect("/view-cart");
                    })
                })
        }

        // if (data.quantity > 1 ){

        // }
        if (!req.session.Cart){
            Cart.create(data, function (error, cart) {
                // console.log(activity);
                if (error) {
                    console.log(error);
                }
                else {
                    console.log("cart", cart, "+++++++");
                    req.session.Cart = cart;

                    console.log(req.session.Cart);
                    return res.redirect("");
                }
            });
        }
        
    },
    viewCart: function (req, res, next) {
        if (!req.session.Cart){
            return res.view('cart/view', {
                cart: null
            });
        }
        Cart.findOne(req.session.Cart.id)
            .populate('activities_ids')
            .exec(function (err, cart) {
            if (err) return res.send("pas de produit dans le panier");
            if (!cart) return res.notFound();
                console.log(cart);
            // This message can contain anything you want!
            // User.message(cart.id, {count: 12, hairColor: 'red'});

            // return res.ok();
            res.view('cart/view',{
                cart: cart
            });
        });
    },
};
