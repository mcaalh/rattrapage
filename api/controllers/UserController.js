/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    'new': function(req,res){
        res.locals.flash = _.clone(req.session.flash); // to have the own copy of sessions.flash
        res.view();
        req.session.flash = {};
    },
	create: function (req, res, next) {
        User.create(req.params.all(), function userCreated(err, user){
            if(err) {
                console.log(err);
                req.session.flash = {
                    err: err
                } 
                return res.redirect('/register');
            }
            user.online = true;
            user.save(function(err, saveUser){
                if (err) {
                    return (next(err));
                }
                // console.log(user.id);
                res.redirect('/login');
                req.session.flash = {};
            })
            User.publishCreate(user);
        });
    },

    getAllUser: function(req, res, next) {
        User.find()
            .exec(function(err, users){
                if (err) {
                    return console.log(err);
                }
                // User.message(foundUser.id, users);
                return res.view('admin/pages/users/list', {
                    
                    users: users
                });
            })
    },

    adresse: function (req, res, next) {
        console.log('adresse');
        console.log(req.session.User);
        User.findOne(req.param('id')).exec(function (err, foundUser) {
            if (err) return res.serverError(err);
            if (!foundUser) return res.notFound();

            // This message can contain anything you want!
            // User.message(foundUser.id, {count: 12, hairColor: 'red'});

            // return res.ok();
            res.view({
                user: foundUser
            });
        });
    },

    infos: function (req, res, next) {
        console.log('infos');
        console.log(req.session.User);
        User.findOne(req.param('id')).exec(function (err, foundUser) {
            if (err) return res.serverError(err);
            if (!foundUser) return res.notFound();

            // This message can contain anything you want!
            // User.message(foundUser.id, {count: 12, hairColor: 'red'});

            // return res.ok();
            res.view({
                user: foundUser
            });
        });
    },
    orders: function (req, res, next) {
        console.log('orders');
        console.log(req.session.User);
        User.findOne(req.param('id')).exec(function (err, foundUser) {
            if (err) return res.serverError(err);
            if (!foundUser) return res.notFound();

            // This message can contain anything you want!
            // User.message(foundUser.id, {count: 12, hairColor: 'red'});

            // return res.ok();
            res.view({
                user: foundUser
            });
        });
    },


    //show profile
    show: function (req, res, next) {
        console.log('show');
        console.log(req.session.User);
        User.findOne(req.param('id')).exec(function(err, foundUser){
            if (err) return res.serverError(err);
            if (!foundUser) return res.notFound();
          
            // This message can contain anything you want!
            // User.message(foundUser.id, {count: 12, hairColor: 'red'});
          
            // return res.ok();
            res.view({
                user: foundUser
            });
          });
    },
    index: function (req, res, next) {
        console.log('index ss');
        User.find(function findUsers (err, users) {
            if (err) {
                return next(err);
            }
            console.log('index found ');
            res.view({
                users: users
            });
        });
    },
    edit: function (req, res, next) {
        // console.log('show');
        User.findOne(req.param('id'), function findUser (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next();
            }
            console.log('found ' + user.id);
            res.view('admin/pages/users/edit',{
                user: user
            });
        });
    },
    update: function (req, res, next) {
        console.log('show');
        User.update(req.param('id'), req.params.all(), function userUpdate (err, user) {
            if (err) {
                return res.redirect('/user/infos/' + req.param('id'));
            }
            // console.log('updated ' + user.id);
            return res.redirect('/user/show/' + req.param('id')); 
        });
    },
    updateDash: function (req, res, next) {
        console.log('show');
        User.update(req.param('id'), req.params.all(), function userUpdate(err, user) {
            if (err) {
                return res.redirect('dashboard/user-edit/' + req.param('id'));
            }
            // console.log('updated ' + user.id);
            return res.redirect('dashboard/users-list');
        });
    },
    subscribe: function (req, res, next) {
        User.find({}, function (err, users) {
            if ( err ) {
                return next(err);
            }
            User.message(users, {count: users.length, hairColor: 'red'});
        })
        
    }
};

