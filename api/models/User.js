var bcrypt = require('bcrypt');

module.exports = {
    // schema: true,
    // connection:'mysql',
    attributes: {
        firstname: {
            type: 'string',
        },

        lastname: {
            type: 'string',
        },
    
        email: {
            type: 'email',
            required: true,
            unique: true
        },

        address: {
            type: 'string',
            unique: true
        },
        
        postcode: {
            type: 'integer',
            maxLength: 5
        },

        ville:{
            type: 'string'
        },

        status: { /* active, pending, deactivate */
            type: 'string',
            // required: true,
        },

        updatedAt: {
            type: 'datetime',
            columnName: 'updated_at'
        },

        deleted: {
            type: 'boolean',
            required: false,
            defaultsTo: false
        },

        cart_id: {
            model: 'cart',
            // required: true
        },

        role_id: {
            model: 'role',
            // required: true
        },

        reservations_ids: {
            collection: 'reservation',
            via: 'users',
            dominant: true
        },
        orders_ids: {
            collection: 'order'
        },

        bills_ids: {
            collection: 'bill',
            via: 'user_id',
        },

        comments_ids: {
            collection: 'comment',
            via: 'user_id',
            dominant: true
        },

        activities_ids: {
            collection: 'activity',
            via: 'users',
            dominant: true
        },

        pictures_ids: {
            collection: 'picture',
            via: 'user_id',
        }, 

        pictures_share_ids: {
            collection: 'picture',
            via: 'users_share_ids',
        },

        avatar: {
            type: 'string',
        },

        admin: {
            type: 'boolean',
            defaultsTo: false
        },

        online: {
            type: 'boolean',
            defaultsTo: false
        },

        password: {
            type: 'string',
            minLength: 6,
            required: true
        },
        toJSON: function () {
            var obj = this.toObject();
            delete obj.password;
            delete obj.confirmation;
            delete obj._csrf;
            return obj;
        }
    },
    beforeCreate: function (user, cb) {
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    console.log(err);
                    cb(err);
                } else {
                    user.password = hash;
                    cb();
                }
            });
        });
    }
};
