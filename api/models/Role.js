/**
 * Role.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    label: {
        type: 'string'
    },
    active: {
        type: 'boolean',
        required: true,
    },

    boAccess: {
        type: 'boolean'
    },

    createdAt: {
        type: 'datetime',
        columnName: 'created_at'
    },

    updatedAt: {
        type: 'datetime',
        columnName: 'updated_at'
    },

    // Add a reference to Users
    users: {
        collection: 'user',
        via: 'role_id'
    },

    // documents: {
    //     collection: 'document',
    //     via: 'roles'
    // }
  }
};

