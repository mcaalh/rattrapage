/**
 * Order.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    price: {
      type: 'float',
      required: true
    },
    cart_id: {
      model: 'cart'
    },
    user_id: {
      model: 'user'
    },
  }
};

